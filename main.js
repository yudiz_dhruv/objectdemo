//user info
const user = {
    name: "Dhruv",
    company: "Yudiz Solutions Ltd.",
    address: {
      personal: {
        line1: "C-Wing, 401",
        line2: "Shubh Vatika, Dindoli",
        city: "Surat",
        state: "Gujarat",
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
      office: {
        city: "City",
        state: "WX",
        area: {
          landmark: "landmark",
        },
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
    },
    contact: {
      phone: {
        home: "1234567890",
        office: "9876543210",
      },
      other: {
        fax: '234'
      },
      email: {
        home: "xxx",
        office: "dhruv.parmar@yudiz.com",
      },
    },
  };

// function to make new object to make desired output.
function doDynamic(obj, obj2, append) {
    obj2 = obj2 || {};
    append = append || "user";

    // it will be executed according to numbers of keys such as name, address, contact.
    for (const key in obj) {

        if (obj.hasOwnProperty(key)) 
        {
            const type = typeof obj[key];
            // condition ? true : false
            const key2 = append ? append + "_" + key : key;

            // if it has value of an object then
            if (type === "string") {
                obj2[key2] = obj[key];
            }
            // if it has nested object such as address
            else if (type === "object") {
                doDynamic(obj[key], obj2, key2);
            }
        }
    }
    return obj2;
}

console.log(doDynamic(user));

// to display in html
// function btnDisplay(){
//     document.getElementById("user_name").innerHTML = "user_name: '" + user.name + "',";
//     document.getElementById("user_name").innerHTML = "user_address_personal_line1: '" + user.address.personal.line1 + "',";
// }
  
//   Output: {
//     user_name: 'Your Name',
//     user_address_personal_line1: '101',
//     user_address_personal_line2: 'street Line',
//     user_address_personal_city: 'NY',
//     user_address_personal_state: 'WX',
//     user_address_personal_coordinates_x: 35.12,
//     user_address_personal_coordinates_y: -21.49,
//     user_address_office_city: 'City',
//     user_address_office_state: 'WX',
//     user_address_office_area_landmark: 'landmark',
//     user_address_office_coordinates_x: 35.12,
//     user_address_office_coordinates_y: -21.49,
//     user_contact_phone_home: 'xxx',
//     user_contact_phone_office: 'yyy',
//     user_contact_other_fax: '234',
//     user_contact_email_home: 'xxx',
//     user_contact_email_office: 'yyy'
//   }